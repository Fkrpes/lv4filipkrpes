﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace lv4_zad1fk
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int i = 0;
            double sumOfColumn = 0;
            int columnCount = data[i].Length;
            int rowCount = data.Length;
            double[] results = new double[columnCount];
            for (i = 0; i < columnCount; i++)
            {
                sumOfColumn = 0;
                for (int j = 0; j < rowCount; j++)
                {
                    sumOfColumn = data[j][i] + sumOfColumn;
                }
                results[i] =sumOfColumn/(double)columnCount;
            }
            
            
            
            
            return results;
        }
    }
}
