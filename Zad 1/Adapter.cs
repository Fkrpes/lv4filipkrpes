﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv4_zad1fk
{
    class Adapter:IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            List<List<double>> copiedData = (List<List<double>>)dataset.GetData();
            int rowCount =copiedData.Count;
            double[][] convertedData= new double[rowCount][];
            int br = 0;
            foreach (List<double> rows in copiedData)
            {
                convertedData[br] = rows.ToArray();
                br++;
            }
            return convertedData;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
