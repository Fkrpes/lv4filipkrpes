﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace lv4_zad1fk
{
    class Program
    {

        static void Main(string[] args)
        {
            Dataset testData = new Dataset(@"C:\Users\Filip\source\repos\lv4_zad1fk\lv4_zad1fk\bin\Debug\netcoreapp3.1\test.csv");
            Analyzer3rdParty test3rdPartyAnalizer = new Analyzer3rdParty() ;
            Adapter testAdapter = new Adapter(test3rdPartyAnalizer);
            int columnCount = testData.GetData().Count;
            
             Console.WriteLine( testAdapter.CalculateAveragePerColumn(testData).ToString());
             Console.WriteLine(testAdapter.CalculateAveragePerRow(testData).ToString());
            
            
        }
    }
}
