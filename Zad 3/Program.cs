﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv4Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> testList = new List<IRentable>();
            testList.Add(new Video("TheBeautyOfGyroZeppely"));
            testList.Add(new Book("Metro2033",10.99));
            RentingConsolePrinter testPrinter = new RentingConsolePrinter();
            testList.Add(new HotItem(new Video("JojoTrilogy")));
            testList.Add(new HotItem(new Book("Metro2034")));

            Console.WriteLine("Items selected:");
            testPrinter.DisplayItems(testList);
            Console.WriteLine("Price of items:");
            testPrinter.PrintTotalPrice(testList);

        }
    }
}
