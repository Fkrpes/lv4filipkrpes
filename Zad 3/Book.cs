﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv4Zad3
{
    class Book:IRentable
    {
        public string title { get; private set; }
        private double baseBookPrice = 3.99;
        public Book(string name) { this.title = name; }
        public Book(string name, double price) { 
            this.title = name;
            this.baseBookPrice = price;        
        }
        public string Description { get { return this.title; } }
        public double CalculatePrice() { return baseBookPrice; }

    }
}
