﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv4Zad3
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
